public class main {
    public static void main(String[] args){
        ShoppingCart cart = new ShoppingCart();
        cart.addProduct(new Product(1, 300, "ananas"));
        cart.addProduct(new Product(2, 89, "apple"));
        cart.addProduct(new Product(3, 299, "coke"));

        System.out.println(cart.getProductsInCard());
        System.out.println(cart.getTotalPrice());
        cart.deleteProduct(3);

        System.out.println(cart.getProductsInCard());
        System.out.println(cart.getTotalPrice());
        System.out.println(cart.getProductPrice(2));
    }
}
