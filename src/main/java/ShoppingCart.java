import java.util.ArrayList;

public class ShoppingCart implements Operations {

    private int productsInCart;
    private int totalPrice;
    private ArrayList<Product> productArray;;

    public ShoppingCart() {
        this.productsInCart = 0;
        this.totalPrice = 0;
        this.productArray = new ArrayList<>();
    }

    public int getProductsInCard() {
        return productsInCart;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public ArrayList<Product> getProductArray() {
        return productArray;
    }

    public int incrementCart(){
        return productsInCart++;
    }

    public int decrementCart(){
        return productsInCart--;
    }

    @Override
    public void addProduct(Product product) {
        this.productArray.add(product);
        this.totalPrice += product.getPrice();
        this.incrementCart();
    }

    @Override
    public void deleteProduct(int id) {
        Product tempProduct = new Product();
        for(Product product : productArray){
            if(product.getId() == id){
                tempProduct = product;
            }
        }
        productArray.remove(tempProduct);
        this.totalPrice -= tempProduct.getPrice();
        this.decrementCart();
    }

    @Override
    public int getProductPrice(int id) {
        for(Product product : productArray){
            if(product.getId() == id){
                return product.getPrice();
            }
        }
        return 0;
    }
}
