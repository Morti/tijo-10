public interface Operations {
    void addProduct(Product product);
    void deleteProduct(int id);
    int getProductPrice(int id);
}
