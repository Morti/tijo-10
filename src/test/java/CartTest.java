import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CartTest {
    private ShoppingCart cart;

    @BeforeEach
    public void init() {
        cart = new ShoppingCart();
        cart.addProduct(new Product(1, 120, "ananas"));
    }

    @Test
    public void addProductTest() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(new Product(1, 120, "ananas"));
        assertEquals(false, shoppingCart.getProductArray().isEmpty(), "adding is not working");
    }

    @Test
    public void deleteProductTest() {
        cart.deleteProduct(1);
        assertEquals(true, cart.getProductArray().isEmpty(), "adding is not working");
    }

    @Test
    public void getProductPriceTest() {
        cart.addProduct(new Product(1, 120, "ananas"));
        assertEquals(120, cart.getProductPrice(1), "getProductPrice is not working");
    }
}
