import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class SortTest {
    private Sort sort;
    @BeforeEach
    public void init() {
        sort = new Sort();
    }
    @Test
    public void arraySortTest() {
        int[] arrayExcepted = new int[]{ 1, 2, 3, 5 }, arrayActual = new int[]{ 3, 2, 1, 5 };
        int[] resultArray = sort.sortArray(arrayActual);
        assertArrayEquals(arrayExcepted, resultArray, "Sort NOT works");
    }
    @Test public void arraySortOneElementTest() {
        int[] arrayExcepted = new int[]{ 1 }, arrayActual = new int[]{ 1 };
        int[] resultArray = sort.sortArray(arrayActual);
        assertArrayEquals(arrayExcepted, resultArray, "Sort one element NOT works");
    }
    @Test public void arraySortTwoElementsTest() {
        int[] arrayExcepted = new int[]{ 1, 2 }, arrayActual = new int[]{ 2, 1 };
        int[] resultArray = sort.sortArray(arrayActual);
        assertArrayEquals(arrayExcepted, resultArray, "Sort two elements NOT works");
    }
    @Test public void arraySortEmptyTest() {
        int[] arrayExcepted = new int[0], arrayActual = new int[0];
        int[] resultArray = sort.sortArray(arrayActual);
        assertArrayEquals(arrayExcepted, resultArray, "Sort empty data NOT works");
    }
}